# Skill assessment task

Thank you for your interest in working with Bember/Prosessor. Here is a description of the skill assessment task. It should take approximately 2-4 hours to complete.

## Practical information

- It is better that we receive a partial answer than no answer at all.
- If you are stuck, feel free to ask us for help. The exercise is meant to be used to give us an idea of how you solve problems and how you work.

## Task overview

We want you to create a mockup of parts of the Bember Loyalty system using the following technology:

- ASP.NET Web API
- Project Orleans
- WebSocket server/SignalR
- Vue.js

You can do this either from scratch, or by cloning this repository: https://bitbucket.org/prosessordev/tech-resource-assessment/src/master/

__Technical hint__: Orleans 1.5 is not compatible with the latest ASP.NET MVC Core 2 Framework. We recommend you use Orleans 2.0. The repository we provide uses Orleans 2.

The repository has the following structure:

- `TRA.DevSilo` - The development silo, should be started
- `TRA.Grains` - Put grain code here
- `TRA.Grains.Interfaces` - Put grain interface code here
- `TRA.Web` - This contains ASP.NET Core, including:
    - Setup of Orleans client using dependency injection
    - Setup of WebSocket server
    - `Index.cshtml` contains information about the project
    - `_Layout.cshtml` contains a link to `Vue.js`

The repository as presented is tested on Windows and Mac OS X.

The HTML/JavaScript part of the code should contain:

- An input form that accepts:
    - amount: number
    - merchant ID: string
    - transaction ID: unique string, such as a guid
- A button to submit the transaction to the web api
- An input form that accepts
    - merchant ID
    - A button to connect a live socket to the server
- Display of this data live:
    - A counter showing the total amount spent for this hour and day (all transactions on this date, not a 24h window) for the connected merchant
    - A counter showing the number of transactions per minute for the whole system
    - A counter showing the number of transactions per minute for the connected merchantId
- The counters are updated "live" over WebSocket/SignalR, meaning it is OK to have some lag on the data (say 5-10 seconds) to allow for doing data aggregation if it makes sense, but lower latency is in general better

The web api should contain a single controller with:

- One endpoint to accept submitting new transactions
- One endpoint to fetch the amount for the transaction

The application layer should be implemented using Project Orleans:

- The transaction grain should persist its state
- Create grains as you see fit to implement other business logic and counters
    - Bonus: How do you solve it so that the counters survive a system restart
- Create grains to communicate with the live socket in a way that allows multiple live sockets be connected simultaneously
    - Hint: Look at Orleans streams
- Create grains to calculate transactions per minute
- We a not looking at a perfect and robust implementation, but something that works. We are not looking for a super performant implementation, but if one is presented that is great.

Root level TASK.md should contain:

- A short description on which technology you picked
- A short description on how you decided to solve the problems given

## Bonus task

- Describe how you would go about creating a solution for making the transaction endpoint idempotent
- Bonus to the bonus task: Present an implementation of the idempotent solution

## Bonus frontend task

- Implement the frontend using Fable/Elmish