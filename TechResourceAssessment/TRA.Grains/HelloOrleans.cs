﻿using Orleans;
using System;
using System.Threading.Tasks;
using TRA.Grains.Interfaces;

namespace TRA.Grains
{
    public class HelloOrleans : Grain, IHelloOrleans
    {
        public Task<string> SayHello(string name)
        {
            return Task.FromResult($"Hello, {name}");
        }
    }
}
