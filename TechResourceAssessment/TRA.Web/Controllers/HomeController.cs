﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TRA.Web.Models;
using Orleans;
using TRA.Grains.Interfaces;

namespace TRA.Web.Controllers
{
    public class HomeController : Controller
    {
        IClusterClient client;

        public HomeController(IClusterClient client)
        {
            this.client = client;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> About()
        {
            var helloGrain = client.GetGrain<IHelloOrleans>(0);
            ViewData["Message"] = await helloGrain.SayHello("ASP.NET Core");

            return View();
        }

        public IActionResult Contact()
        {
            
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
