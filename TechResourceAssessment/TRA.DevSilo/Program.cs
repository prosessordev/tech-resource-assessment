﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;
using System;
using System.Net;
using System.Threading.Tasks;
using TRA.Grains;

namespace TRA.DevSilo
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var startTaskResult = Start();
                Task.WaitAll(startTaskResult);
            } catch (Exception ex)
            {
                Console.Error.WriteLine(ex.ToString());
                Console.ReadLine();
            }            
        }

        static private async Task Start()
        {
            var host = await StartSilo();
            Console.WriteLine("Press ENTER to terminate silo...");
            Console.ReadLine();

            await host.StopAsync();
        }

        private static async Task<ISiloHost> StartSilo()
        {
            // define the cluster configuration            
            var builder = new SiloHostBuilder()
                .UseLocalhostClustering()
                // Add assemblies to scan for grains and serializers.
                // For more info read the Application Parts section
                .ConfigureApplicationParts(parts =>
                    parts.AddApplicationPart(typeof(HelloOrleans).Assembly)
                         .WithReferences())
                // Configure logging with any logging framework that supports Microsoft.Extensions.Logging.
                // In this particular case it logs using the Microsoft.Extensions.Logging.Console package.
                .ConfigureLogging(logging => logging.AddConsole());

            var host = builder.Build();
            await host.StartAsync();
            return host;
        }
    }
}
