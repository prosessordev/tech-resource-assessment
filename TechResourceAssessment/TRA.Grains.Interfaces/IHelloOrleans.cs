﻿using Orleans;
using System;
using System.Threading.Tasks;

namespace TRA.Grains.Interfaces
{
    public interface IHelloOrleans : IGrainWithIntegerKey
    {
        Task<string> SayHello(string name);
        
    }
}
